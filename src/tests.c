#include "tests.h"
#include <stdlib.h>
#define HEAP_SIZE 4096

struct block_header* block_get_header(void* contents);

void error( const char* msg) {
    fprintf(stderr, "test failed. %s%s", msg,"\n");
    abort();
}

void success( const char* testName, const char* successMessage ) {
    fprintf(stdout, "%s%s%s", testName, successMessage,"\n");
}

static void complete( void *heap, int query, const char *const testName) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = query}).bytes);
    success(testName, " is successful!");
}

static void print_and_debug_heap(const void *const heap, const char *const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

void test1(void) {
    void* test_heap = heap_init(HEAP_SIZE);
    if (test_heap == NULL) error("Heap is not initialized." );
    print_and_debug_heap(test_heap, "Heap after init");
    void* allocate_memory = _malloc(2048);
    if (allocate_memory == NULL) error("Memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(allocate_memory);
    print_and_debug_heap(test_heap, "Heap after free mem");
    complete(test_heap, 4096, "Test 1");
}

void test2(void) {
    void* test_heap = heap_init(HEAP_SIZE);
    if (test_heap == NULL) error("Heap is not initialized." );
    print_and_debug_heap(test_heap, "Heap after init");
    void* allocate_memory1 = _malloc(1024);
    void* allocate_memory2 = _malloc(1024);
    if (allocate_memory1 == NULL || allocate_memory2 == NULL) error("Memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(allocate_memory1);
    if ( allocate_memory2 == NULL ) error("Release of the first damaged the second.");
    print_and_debug_heap(test_heap, "Heap after free mem");
    _free(allocate_memory2);
    complete(test_heap, 4096, "Test 2");
}

void test3(void) {
    void* test_heap = heap_init(HEAP_SIZE);
    if (test_heap == NULL) error("Heap is not initialized." );
    print_and_debug_heap(test_heap, "Heap after init");
    void* allocate_memory1 = _malloc(1024);
    void* allocate_memory2 = _malloc(1024);
    void* allocate_memory3 = _malloc(1024);
    if ( allocate_memory1 == NULL ) error("First memory is not allocated.");
    if ( allocate_memory2 == NULL || allocate_memory3 == NULL) error(" Second memory is not allocated.");
    if ( allocate_memory3 == NULL) error("Third memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    _free(allocate_memory1);
    _free(allocate_memory3);
    if ( allocate_memory2 == NULL ) error("Release of the first damaged the second.");
    print_and_debug_heap(test_heap, "Heap after free mem");
    complete(test_heap, 4096, "Test 3");
}

void test4(void) {
    void* test_heap = heap_init(1);
    if ( test_heap == NULL ) error("Heap is not initialized." );
    print_and_debug_heap(test_heap, "Heap after init");
    void* allocate_memory1 = _malloc(8192);
    void* allocate_memory2 = _malloc(16384);
    if (allocate_memory1 == NULL) error("First memory is not allocated.");
    if (allocate_memory2 == NULL) error("Second memory is not allocated.");
    print_and_debug_heap(test_heap, "Heap after alloc");
    struct block_header* header1 = block_get_header(allocate_memory1);
    struct block_header* header2 = block_get_header(allocate_memory2);
    if ( header1 == NULL || header1->next != header2 ) error("Headers are not linked.");
    complete(test_heap,(int) (header1->capacity.bytes + header2->capacity.bytes + ( (struct block_header*) test_heap)->capacity.bytes ), "Test 4");
}


void test5(void) {
    void* test_heap = heap_init(1);
    if (test_heap == NULL) error("Heap is not initialized." );
    print_and_debug_heap(test_heap, "Heap after init");
    void* allocate_memory1 = _malloc(8192);
    if ( allocate_memory1 == NULL ) error("First memory is not allocated.");
    struct block_header* header1 = block_get_header(allocate_memory1);
    if ( header1 == NULL ) error("Header is not find.");
    print_and_debug_heap(test_heap, "Heap after alloc.");
    void* region = mmap( header1->contents + header1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0 );
    void* allocate_memory2 = _malloc(8192);
    struct block_header* header2 = block_get_header(allocate_memory2);
    if ( allocate_memory2 == NULL) error("Second memory is not allocated");
    print_and_debug_heap(test_heap, "Heap after second malloc");
    _free(allocate_memory1);
    _free(allocate_memory2);
    print_and_debug_heap(test_heap, "Heap after realising");
    munmap(region, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    complete(test_heap, (int) (header1->capacity.bytes + header2->capacity.bytes + ( (struct block_header*) test_heap)->capacity.bytes) , "Test 5");
}
